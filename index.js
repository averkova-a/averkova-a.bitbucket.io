const API_URL = 'https://api.github.com/';
const rootElement = document.getElementById('root');
const loadingElement = document.getElementById('loading-overlay');
const fightersDetailsMap = new Map();

// Класс-конструктор создания нового бойца. Через fighterService.getFighterInfo получаем дополнительную информацию.
class Fighter {
  constructor(info) {
    this._id = info._id;
    this.name = info.name;
    this.health = info.health;
    this.attack = info.attack;
    this.defense = info.defense;
  }

  getHitPower(attack) {
      return attack * Math.random() + 1; // [1;2)
  }

  getBlockPower(defense) {
      return defense * Math.random() + 1; // [1;2)
  }
}
// Запрашивает информацию и создаёт экземпляр класса.
async function newFighter(id) {
  const info = await fighterService.getFighterInfo(id);
  return new Fighter(info);
}


function callApi(endpoind, method) {
    const url = API_URL + endpoind
    const options = {
      method
    };
  
    return fetch(url, options)
      .then(response => 
        response.ok 
          ? response.json() 
          : Promise.reject(Error('Failed to load'))
      )
      .catch(error => { throw error });
}; 

class FighterService {
    async getFighters() {
      try {
        const endpoint = 'repositories/186232982/contents/resources/api/fighters.json';
        const apiResult = await callApi(endpoint, 'GET');
  
        return JSON.parse(atob(apiResult.content));
      } catch (error) {
        throw error;
      }
    }

    async getFighterInfo(id) {
        try {
            const endpoint = 'repositories/186232982/contents/resources/api/details/fighter/' + id +'.json';
            const apiResult = await callApi(endpoint, 'GET');
      
            return JSON.parse(atob(apiResult.content));
          } catch (error) {
            throw error;
          }
    }
}

const fighterService = new FighterService;

class View {
    element;
  
    createElement({ tagName, className = '', attributes = {} }) {
      const element = document.createElement(tagName);
      element.classList.add(className);
      
      Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
  
      return element;
    }
}

class FighterView extends View {
    constructor(fighter, handleClick) {
      super();
  
      this.createFighter(fighter, handleClick);
    }
  
    createFighter(fighter, handleClick) {
      const { name, source } = fighter;
      const nameElement = this.createName(name);
      const imageElement = this.createImage(source);
  
      this.element = this.createElement({ tagName: 'div', className: 'fighter' });
      this.element.append(imageElement, nameElement);
      this.element.addEventListener('click', event => handleClick(event, fighter), false);
    }
  
    createName(name) {
      const nameElement = this.createElement({ tagName: 'span', className: 'name' });
      nameElement.innerText = name;
  
      return nameElement;
    }
  
    createImage(source) {
      const attributes = { src: source };
      const imgElement = this.createElement({
        tagName: 'img',
        className: 'fighter-image',
        attributes
      });
  
      return imgElement;
    }
}

class FightersView extends View {
    constructor(fighters) {
        super();
        this.handleClick = this.handleFighterClick.bind(this);
        this.createFighters(fighters);
    }
  
    fightersDetailsMap = new Map();
  
    createFighters(fighters) {
      const fighterElements = fighters.map(fighter => {
        const fighterView = new FighterView(fighter, this.handleClick);
        return fighterView.element;
      });
  
      this.element = this.createElement({ tagName: 'div', className: 'fighters' });
      this.element.append(...fighterElements);
    }
  
    handleFighterClick(event, fighter) {
      this.fightersDetailsMap.set(fighter._id, fighter);
      console.log('clicked')
      // get from map or load info and add to fightersMap
      // show modal with fighter info
      // allow to edit health and power in this modal
    }
}

class App {
    constructor() {
      this.startApp();
    }
  
    static rootElement = document.getElementById('root');
    static loadingElement = document.getElementById('loading-overlay');
  
    async startApp() {
      try {
        App.loadingElement.style.visibility = 'visible';
        
        const fighters = await fighterService.getFighters();
        const fightersView = new FightersView(fighters);
        const fightersElement = fightersView.element;
        console.log(await newFighter(1), await newFighter(2));
  
        App.rootElement.appendChild(fightersElement);
      } catch (error) {
        console.warn(error);
        App.rootElement.innerText = 'Failed to load data';
      } finally {
        App.loadingElement.style.visibility = 'hidden';
      }
    }
}

new App();